#include  //includes library for the sensor

#define DHTPIN 8 // what digital pin we are using
#define DHTTYPE DHT22 // what type of DHT sensor we are using

DHT dht(DHTPIN, DHTTYPE); // creates object called dht we can call later

float t; //variable for temperature
float h; //variable for humidity

void setup() {
	Serial.begin(9600); // starts serial communication
	dht.begin(); // starts listening to sensor
}

void loop() {
	t = dht.readTemperature();
	h = dht.readHumidity();
	Serial.println("Temperature and humidity: ");
	Serial.println(t);
	Serial.println(h);
	delay(2000);
}