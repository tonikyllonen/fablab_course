#define BLYNK_TEMPLATE_ID "TMPL4L3SSKZTF"
#define BLYNK_TEMPLATE_NAME "Academy Demo"
#define BLYNK_AUTH_TOKEN "*******************"
#define DHTPIN 4
#define DHTTYPE DHT11
#define BLYNK_PRINT Serial
#define LED 2

#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>
#include <DHT.h>

int t;
int h;

char ssid[] = "*******************";
char pass[] = "*******************";

DHT dht(DHTPIN, DHTTYPE);
BlynkTimer timer;

void setup()
{
	pinMode(LED, OUTPUT);
	pinMode(DHTPIN, INPUT);
	Serial.begin(9600);
	dht.begin();
	Blynk.begin(BLYNK_AUTH_TOKEN, ssid, pass);
	timer.setInterval(1000L, sendData);

}

void sendData() {
	t = dht.readTemperature();
	h = dht.readHumidity();
	Serial.println(t);
	Serial.println(h);
	Blynk.virtualWrite(V0, t);
	Blynk.virtualWrite(V1, h);
}

void loop()
{
	
	Blynk.run();
	timer.run();
}
	