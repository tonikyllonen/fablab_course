#define DHTPIN 6
#define DHTTYPE DHT22
#define LED 4

#include <DHT.h>

int t;
int h;

DHT dht(DHTPIN, DHTTYPE);

void setup()
{
  pinMode(LED, OUTPUT);
  pinMode(DHTPIN, INPUT);
  Serial.begin(9600);
  dht.begin();
  digitalWrite(LED, HIGH);
}

void sendData() {
  t = dht.readTemperature();
  h = dht.readHumidity();
  Serial.println(t);
  //Serial.println(h);
}

void loop()
{
  sendData();
  if (digitalRead(LED) == HIGH) {
    digitalWrite(LED, LOW);
  }
  else {
    digitalWrite(LED, HIGH);
  }
  delay(1000);
}
