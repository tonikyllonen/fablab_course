#include <Keypad.h>

const int ROWS = 3;
const int COLUMNS = 7;

//const int pin = 5;

char buttons[ROWS][COLUMNS] = {
  {'R1','R2','R3','R4','R5','LS','RS'},
  {'1','2','3','4','5','6','7'},
  {'8','9','10','11','12','13','14'}
};

byte row_pins[ROWS] = {30, 31, 0};
byte column_pins[COLUMNS] = {5, 6, 7, 4, 3, 2, 1};

void setup() {
  //pinMode(pin, INPUT_PULLUP);
  Serial.begin(9600);
}

Keypad keypad = Keypad(makeKeymap(buttons), row_pins, column_pins, ROWS, COLUMNS);

void loop() {
  //int button = digitalRead(pin);
  char button = keypad.getKey();
  //Serial.println(button);
  if (button != NO_KEY){
    Serial.println(button);
  }
}
