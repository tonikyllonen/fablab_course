EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L drv8825:DRV8825_STEPPER_MOTOR_DRIVER_CARRIER U2
U 1 1 64222942
P 6600 3400
F 0 "U2" H 6600 4265 50  0000 C CNN
F 1 "DRV8825_STEPPER_MOTOR_DRIVER_CARRIER" H 6600 4174 50  0000 C CNN
F 2 "drv8825:IC_DRV8825_STEPPER_MOTOR_DRIVER_CARRIER" H 6600 3400 50  0001 L BNN
F 3 "" H 6600 3400 50  0001 L BNN
F 4 "DRV8825 STEPPER MOTOR DRIVER CARRIER" H 6600 3400 50  0001 L BNN "MP"
F 5 "None" H 6600 3400 50  0001 L BNN "PRICE"
F 6 "None" H 6600 3400 50  0001 L BNN "PACKAGE"
F 7 "Unavailable" H 6600 3400 50  0001 L BNN "AVAILABILITY"
F 8 "Pololu" H 6600 3400 50  0001 L BNN "MF"
F 9 "Stepper motor controler; IC: DRV8825; 1.5A; Uin mot: 8.2÷45V" H 6600 3400 50  0001 L BNN "DESCRIPTION"
	1    6600 3400
	1    0    0    -1  
$EndComp
Text GLabel 4850 1850 0    50   Input ~ 0
12V
Text GLabel 5250 1850 2    50   Input ~ 0
GND
Wire Wire Line
	5000 1850 4950 1850
Wire Wire Line
	5100 1850 5250 1850
Wire Wire Line
	5100 1850 5100 2150
Wire Wire Line
	5100 2150 7600 2150
Wire Wire Line
	7600 2150 7600 3900
Wire Wire Line
	7600 3900 7300 3900
Connection ~ 5100 1850
Wire Wire Line
	4950 1850 4950 2250
Wire Wire Line
	4950 2250 7500 2250
Wire Wire Line
	7500 2250 7500 2800
Wire Wire Line
	7500 2800 7300 2800
Connection ~ 4950 1850
Wire Wire Line
	4950 1850 4850 1850
Wire Wire Line
	5400 3050 5400 3100
Wire Wire Line
	4500 2850 5100 2850
Wire Wire Line
	5100 2850 5100 2650
Connection ~ 5100 2150
$Comp
L arduino:Arduino_Micro U1
U 1 1 6421BD91
P 3900 3350
F 0 "U1" H 3900 4439 60  0000 C CNN
F 1 "Arduino_Micro" H 3900 4333 60  0000 C CNN
F 2 "arduino:Arduino_Micro" H 3900 2400 60  0001 C CNN
F 3 "https://store.arduino.cc/usa/arduino-micro" H 4050 2300 60  0001 C CNN
	1    3900 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3050 5400 3050
Wire Wire Line
	5700 3400 5900 3400
$Comp
L radio-cache:fab_Conn_01x04_Male J2
U 1 1 6423457D
P 7900 3450
F 0 "J2" H 7872 3332 50  0000 R CNN
F 1 "Motor1" H 7872 3423 50  0000 R CNN
F 2 "fab:Header_SMD_01x04_P2.54mm_Horizontal_Male" H 7900 3450 50  0001 C CNN
F 3 "" H 7900 3450 50  0001 C CNN
	1    7900 3450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7300 3600 7500 3600
Wire Wire Line
	7500 3600 7500 3550
Wire Wire Line
	7500 3550 7700 3550
Wire Wire Line
	7300 3700 7550 3700
Wire Wire Line
	7550 3700 7550 3650
Wire Wire Line
	7550 3650 7700 3650
Wire Wire Line
	7300 4000 7600 4000
Wire Wire Line
	7600 4000 7600 3900
Connection ~ 7600 3900
Wire Wire Line
	5000 1300 5000 1600
Wire Wire Line
	5100 1300 5100 1750
Wire Wire Line
	5000 1600 5750 1600
Connection ~ 5000 1600
Wire Wire Line
	5000 1600 5000 1850
Wire Wire Line
	5100 1750 5750 1750
Wire Wire Line
	5750 1750 5750 1800
Connection ~ 5100 1750
Wire Wire Line
	5100 1750 5100 1850
$Comp
L buck-convert:Mini360 U3
U 1 1 64233B7A
P 6350 1700
F 0 "U3" H 6350 2187 60  0000 C CNN
F 1 "Mini360" H 6350 2081 60  0000 C CNN
F 2 "buck-convert:Mini360_step-down" H 6350 1700 60  0001 C CNN
F 3 "" H 6350 1700 60  0001 C CNN
	1    6350 1700
	1    0    0    -1  
$EndComp
Text GLabel 6950 1800 2    50   Input ~ 0
GND
Text GLabel 6950 1600 2    50   Input ~ 0
5V
Wire Wire Line
	5650 3300 5900 3300
$Comp
L drv8825:DRV8825_STEPPER_MOTOR_DRIVER_CARRIER U4
U 1 1 64244039
P 6600 5050
F 0 "U4" H 6600 5915 50  0000 C CNN
F 1 "DRV8825_STEPPER_MOTOR_DRIVER_CARRIER" H 6600 5824 50  0000 C CNN
F 2 "drv8825:IC_DRV8825_STEPPER_MOTOR_DRIVER_CARRIER" H 6600 5050 50  0001 L BNN
F 3 "" H 6600 5050 50  0001 L BNN
F 4 "DRV8825 STEPPER MOTOR DRIVER CARRIER" H 6600 5050 50  0001 L BNN "MP"
F 5 "None" H 6600 5050 50  0001 L BNN "PRICE"
F 6 "None" H 6600 5050 50  0001 L BNN "PACKAGE"
F 7 "Unavailable" H 6600 5050 50  0001 L BNN "AVAILABILITY"
F 8 "Pololu" H 6600 5050 50  0001 L BNN "MF"
F 9 "Stepper motor controler; IC: DRV8825; 1.5A; Uin mot: 8.2÷45V" H 6600 5050 50  0001 L BNN "DESCRIPTION"
	1    6600 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 5650 7600 5550
Wire Wire Line
	7300 5650 7600 5650
Wire Wire Line
	7550 5300 7700 5300
Wire Wire Line
	7550 5350 7550 5300
Wire Wire Line
	7300 5350 7550 5350
Wire Wire Line
	7500 5200 7700 5200
Wire Wire Line
	7500 5250 7500 5200
Wire Wire Line
	7300 5250 7500 5250
$Comp
L radio-cache:fab_Conn_01x04_Male J4
U 1 1 64244043
P 7900 5100
F 0 "J4" H 7872 4982 50  0000 R CNN
F 1 "Motor2" H 7872 5073 50  0000 R CNN
F 2 "fab:Header_SMD_01x04_P2.54mm_Horizontal_Male" H 7900 5100 50  0001 C CNN
F 3 "" H 7900 5100 50  0001 C CNN
	1    7900 5100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7600 5550 7300 5550
Wire Wire Line
	5700 4400 5700 3400
Wire Wire Line
	5650 4450 5650 3300
Wire Wire Line
	7500 2250 7950 2250
Wire Wire Line
	7950 2250 7950 4450
Wire Wire Line
	7950 4450 7300 4450
Connection ~ 7500 2250
Wire Wire Line
	7600 2150 8050 2150
Connection ~ 7600 2150
Wire Wire Line
	8050 5550 7600 5550
Wire Wire Line
	8050 2150 8050 5550
Connection ~ 7600 5550
Wire Wire Line
	3000 4950 5900 4950
Wire Wire Line
	3050 5050 5900 5050
Wire Wire Line
	5400 3200 5400 3800
Wire Wire Line
	5400 4850 5900 4850
Connection ~ 5400 3200
Wire Wire Line
	5900 4750 5400 4750
Connection ~ 5400 4750
Wire Wire Line
	5400 4750 5400 4850
Wire Wire Line
	5400 3200 5900 3200
Wire Wire Line
	5900 3100 5400 3100
Connection ~ 5400 3100
Wire Wire Line
	5400 3100 5400 3200
$Comp
L fab:Conn_01x03_Male J3
U 1 1 6425C68F
P 4050 1750
F 0 "J3" H 4022 1682 50  0000 R CNN
F 1 "Servo" H 4022 1773 50  0000 R CNN
F 2 "fab:Header_SMD_01x03_P2.54mm_Horizontal_Male" H 4050 1750 50  0001 C CNN
F 3 "~" H 4050 1750 50  0001 C CNN
	1    4050 1750
	-1   0    0    1   
$EndComp
Text GLabel 3850 1850 0    50   Input ~ 0
GND
Text GLabel 3850 1750 0    50   Input ~ 0
5V
Wire Wire Line
	3300 3850 3100 3850
Wire Wire Line
	7300 5050 7550 5050
Wire Wire Line
	7550 5050 7550 5000
Wire Wire Line
	7550 5000 7700 5000
Wire Wire Line
	7300 4950 7500 4950
Wire Wire Line
	7500 4950 7500 5100
Wire Wire Line
	7500 5100 7700 5100
Wire Wire Line
	7300 3300 7500 3300
Wire Wire Line
	7500 3300 7500 3450
Wire Wire Line
	7500 3450 7700 3450
Wire Wire Line
	7300 3400 7550 3400
Wire Wire Line
	7550 3400 7550 3350
Wire Wire Line
	7550 3350 7700 3350
Wire Wire Line
	3300 3050 3200 3050
Wire Wire Line
	3200 3050 3200 2150
Wire Wire Line
	3200 2150 5100 2150
Wire Wire Line
	3000 3450 3300 3450
Wire Wire Line
	3000 3450 3000 4950
Wire Wire Line
	3050 3350 3300 3350
Wire Wire Line
	3050 3350 3050 5050
Wire Wire Line
	5650 4450 3150 4450
Wire Wire Line
	3150 4450 3150 3650
Wire Wire Line
	3150 3650 3300 3650
Wire Wire Line
	5700 4400 3200 4400
Wire Wire Line
	3200 4400 3200 3550
Wire Wire Line
	3200 3550 3300 3550
$Comp
L fab:C C1
U 1 1 642C2179
P 5100 2500
F 0 "C1" V 4848 2500 50  0000 C CNN
F 1 "C" V 4939 2500 50  0000 C CNN
F 2 "fab:C_1206" H 5138 2350 50  0001 C CNN
F 3 "" H 5100 2500 50  0001 C CNN
	1    5100 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	4950 2250 4950 2500
Connection ~ 4950 2250
Wire Wire Line
	5100 2650 5250 2650
Wire Wire Line
	5250 2650 5250 2500
Connection ~ 5100 2650
Wire Wire Line
	5100 2650 5100 2150
Wire Wire Line
	5900 3800 5400 3800
Connection ~ 5400 3800
Wire Wire Line
	5400 3800 5400 4750
Wire Wire Line
	5900 5450 5400 5450
Wire Wire Line
	5400 5450 5400 4850
Connection ~ 5400 4850
Wire Wire Line
	3850 1650 3100 1650
Wire Wire Line
	3100 1650 3100 3850
$Comp
L fab:Conn_01x01_Male J1
U 1 1 642D9117
P 5000 1100
F 0 "J1" V 5062 1144 50  0000 L CNN
F 1 "Conn_01x01_Male" V 5153 1144 50  0000 L CNN
F 2 "fab:Header_SMD_01x01_P2.54mm_Horizontal_Male" H 5000 1100 50  0001 C CNN
F 3 "~" H 5000 1100 50  0001 C CNN
	1    5000 1100
	0    1    1    0   
$EndComp
$Comp
L fab:Conn_01x01_Male J5
U 1 1 642D9E79
P 5100 1100
F 0 "J5" V 5162 1144 50  0000 L CNN
F 1 "Conn_01x01_Male" V 5253 1144 50  0000 L CNN
F 2 "fab:Header_SMD_01x01_P2.54mm_Horizontal_Male" H 5100 1100 50  0001 C CNN
F 3 "~" H 5100 1100 50  0001 C CNN
	1    5100 1100
	0    1    1    0   
$EndComp
$EndSCHEMATC
