#include <Adafruit_NeoPixel.h> //include NeoPixel library

#define PIN 8 //Pin where LED's data in is connected
#define NUM 1 //How many LEDs

Adafruit_NeoPixel pixels(NUM, PIN, NEO_RGB + NEO_KHZ800); //defines LED object so we can call it later

void setup() {
  pixels.begin(); //LED starts listening for commands
}

void loop() {

  // This part of the code loops the LED effects: OFF, RED, GREEN, BLUE and WHITE

  pixels.clear(); //sets LED off
  pixels.show();
  delay(4000); //wait
  pixels.setPixelColor(0, pixels.Color(255,0,0)); //sets LED red
  pixels.show(); //update LED colors
  delay(4000); //wait
  pixels.setPixelColor(0, pixels.Color(0,255,0)); //sets LED green
  pixels.show(); //update LED colors
  delay(4000); //wait
  pixels.setPixelColor(0, pixels.Color(0,0,255)); //sets LED blue
  pixels.show(); //update LED colors
  delay(4000);//wait
  pixels.setPixelColor(0, pixels.Color(255,255,255)); //sets LED white
  pixels.show(); //update LED colors
  delay(4000);//wait
}
