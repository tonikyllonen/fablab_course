EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:Conn_01x02_Male J2
U 1 1 63FF7B2C
P 3650 2450
F 0 "J2" H 3622 2424 50  0000 R CNN
F 1 "output" H 3622 2333 50  0000 R CNN
F 2 "fab:Header_SMD_01x02_P2.54mm_Horizontal_Male" H 3650 2450 50  0001 C CNN
F 3 "~" H 3650 2450 50  0001 C CNN
	1    3650 2450
	-1   0    0    -1  
$EndComp
$Comp
L fab:Conn_01x03_Male J1
U 1 1 63FF848A
P 2100 2850
F 0 "J1" H 2072 2782 50  0000 R CNN
F 1 "input" H 2072 2873 50  0000 R CNN
F 2 "fab:Header_SMD_01x03_P2.54mm_Horizontal_Male" H 2100 2850 50  0001 C CNN
F 3 "~" H 2100 2850 50  0001 C CNN
	1    2100 2850
	1    0    0    1   
$EndComp
$Comp
L fab:MOSFET_N-CH_30V_1.7A Q1
U 1 1 63FF90D5
P 2850 2700
F 0 "Q1" H 2958 2742 45  0000 L CNN
F 1 "MOSFET_N-CH_30V_1.7A" H 2958 2658 45  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2880 2850 20  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/NDS355AN-D.PDF" H 2850 2700 50  0001 C CNN
	1    2850 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 2850 2650 2800
Wire Wire Line
	2850 2950 2850 2900
Wire Wire Line
	2850 2500 3350 2500
Wire Wire Line
	3350 2500 3350 2550
Wire Wire Line
	3350 2550 3450 2550
Wire Wire Line
	2450 2750 2300 2750
Wire Wire Line
	2450 2450 2550 2450
Wire Wire Line
	2450 2750 2450 2450
Wire Wire Line
	2300 2950 2550 2950
Wire Wire Line
	2300 2850 2650 2850
$Comp
L fab:D_Schottky D1
U 1 1 63FFE59E
P 2550 2750
F 0 "D1" V 2504 2829 50  0000 L CNN
F 1 "D_Schottky" V 2595 2829 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123" H 2550 2750 50  0001 C CNN
F 3 "" H 2550 2750 50  0001 C CNN
	1    2550 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	2550 2600 2550 2450
Connection ~ 2550 2450
Wire Wire Line
	2550 2450 3450 2450
Wire Wire Line
	2550 2950 2550 2900
Connection ~ 2550 2950
Wire Wire Line
	2550 2950 2850 2950
$EndSCHEMATC
