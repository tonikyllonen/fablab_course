import board
import digitalio
import time

r = digitalio.DigitalInOut(board.LED_RED)
g = digitalio.DigitalInOut(board.LED_GREEN)
b = digitalio.DigitalInOut(board.LED_BLUE)

r.direction = digitalio.Direction.OUTPUT
g.direction = digitalio.Direction.OUTPUT
b.direction = digitalio.Direction.OUTPUT


while True:
    b.value = True
    r.value = False
    time.sleep(1)
    r.value = True
    g.value = False
    time.sleep(1)
    g.value = True
    b.value = False
    time.sleep(1)

