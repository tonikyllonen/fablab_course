import time
import board
import neopixel

pixel_pin = board.NEOPIXEL
num_pixels = 3

pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=0.5, auto_write=False)

while True:
    for i in range(150):
        pixels.fill((0, i, 0))
        pixels.show()
        time.sleep(0.02)

    for i in reversed(range(150)):
        pixels.fill((0, i, 0))
        pixels.show()
        time.sleep(0.02)
    time.sleep(0.1)

