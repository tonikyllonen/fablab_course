EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:C C1
U 1 1 641C757D
P 3100 3750
F 0 "C1" H 3215 3796 50  0000 L CNN
F 1 "C" H 3215 3705 50  0000 L CNN
F 2 "fab:C_1206" H 3138 3600 50  0001 C CNN
F 3 "" H 3100 3750 50  0001 C CNN
	1    3100 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3050 4000 2800
Wire Wire Line
	4000 2800 3100 2800
Wire Wire Line
	3100 2800 3100 3600
Wire Wire Line
	3100 3900 3100 4700
Wire Wire Line
	3100 4700 4000 4700
Wire Wire Line
	4000 4700 4000 4450
$Comp
L fab:LED D1
U 1 1 641C84DB
P 6450 4550
F 0 "D1" H 6443 4295 50  0000 C CNN
F 1 "LED" H 6443 4386 50  0000 C CNN
F 2 "fab:LED_1206" H 6450 4550 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 6450 4550 50  0001 C CNN
	1    6450 4550
	-1   0    0    1   
$EndComp
$Comp
L fab:R R1
U 1 1 641C9346
P 6100 4550
F 0 "R1" V 6307 4550 50  0000 C CNN
F 1 "R" V 6216 4550 50  0000 C CNN
F 2 "fab:R_1206" V 6030 4550 50  0001 C CNN
F 3 "~" H 6100 4550 50  0001 C CNN
	1    6100 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 4550 6300 4550
Connection ~ 4000 4700
Wire Wire Line
	4000 4700 4000 5050
Wire Wire Line
	5550 5050 5550 3700
Wire Wire Line
	5550 3700 5900 3700
Wire Wire Line
	5900 3500 5550 3500
Wire Wire Line
	5550 3500 5550 2800
Wire Wire Line
	5550 2800 4000 2800
Connection ~ 4000 2800
$Comp
L fab:Conn_01x03_Male J2
U 1 1 641D073C
P 6100 2300
F 0 "J2" H 6072 2232 50  0000 R CNN
F 1 "Conn_01x03_Male" H 6072 2323 50  0000 R CNN
F 2 "fab:Header_SMD_01x03_P2.54mm_Horizontal_Male" H 6100 2300 50  0001 C CNN
F 3 "~" H 6100 2300 50  0001 C CNN
	1    6100 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 5050 2850 5050
Wire Wire Line
	2850 5050 2850 2300
Wire Wire Line
	2850 2300 3450 2300
Connection ~ 4000 5050
Wire Wire Line
	5900 2400 4000 2400
Wire Wire Line
	4000 2400 4000 2650
Wire Wire Line
	5900 2200 5200 2200
Wire Wire Line
	4600 2200 4600 3450
Text Label 3450 2050 0    50   ~ 0
GND
Wire Wire Line
	3450 2050 3450 2300
Connection ~ 3450 2300
Wire Wire Line
	3450 2300 5900 2300
Text Label 4200 2650 0    50   ~ 0
VCC
Wire Wire Line
	4200 2650 4000 2650
Connection ~ 4000 2650
Wire Wire Line
	4000 2650 4000 2800
Text Label 5200 2050 0    50   ~ 0
UPDI
Wire Wire Line
	5200 2050 5200 2200
Connection ~ 5200 2200
Wire Wire Line
	5200 2200 4600 2200
Wire Wire Line
	5300 3400 5900 3400
Wire Wire Line
	5200 3550 5200 3300
Wire Wire Line
	5200 3300 5900 3300
Text Label 4900 3400 0    50   ~ 0
TX
Wire Wire Line
	4900 3400 4900 3550
Wire Wire Line
	4900 3550 5200 3550
Text Label 5400 3600 0    50   ~ 0
RX
Wire Wire Line
	5400 3600 5300 3600
Wire Wire Line
	5300 3600 5300 3400
Connection ~ 5300 3600
$Comp
L fab:Conn_01x08_Male J1
U 1 1 641DD234
P 6100 3700
F 0 "J1" H 6072 3582 50  0000 R CNN
F 1 "Conn_01x08_Male" H 6072 3673 50  0000 R CNN
F 2 "fab:Header_SMD_01x08_P2.54mm_Horizontal_Male" H 6100 3700 50  0001 C CNN
F 3 "~" H 6100 3700 50  0001 C CNN
	1    6100 3700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 3950 4900 3950
Wire Wire Line
	4900 3950 4900 3550
Connection ~ 4900 3550
Wire Wire Line
	5300 3600 5300 4050
Wire Wire Line
	4600 4050 5300 4050
Wire Wire Line
	4000 5050 5550 5050
$Comp
L fab:Microcontroller_ATtiny412_SSFR U1
U 1 1 641C6B1B
P 4000 3750
F 0 "U1" H 3470 3796 50  0000 R CNN
F 1 "Microcontroller_ATtiny412_SSFR" H 3470 3705 50  0000 R CNN
F 2 "fab:SOIC-8_3.9x4.9mm_P1.27mm" H 4000 3750 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf" H 4000 3750 50  0001 C CNN
	1    4000 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3550 4850 3550
Wire Wire Line
	4850 3550 4850 3800
Wire Wire Line
	4850 3800 5900 3800
Wire Wire Line
	4600 3650 4800 3650
Wire Wire Line
	4800 3650 4800 4000
Wire Wire Line
	4800 4000 5650 4000
Wire Wire Line
	4600 3750 4750 3750
Wire Wire Line
	4750 3750 4750 3900
Wire Wire Line
	4750 3900 5900 3900
Wire Wire Line
	6600 4550 6600 5050
Wire Wire Line
	6600 5050 5550 5050
Connection ~ 5550 5050
Wire Wire Line
	5650 4000 5650 4550
Wire Wire Line
	5650 4550 5950 4550
Connection ~ 5650 4000
Wire Wire Line
	5650 4000 5900 4000
$EndSCHEMATC
